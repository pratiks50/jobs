<?php
include 'extra_jobs_fields.php';

function scroll_chr_nav(){
	?>
	<script type="text/javascript">
		jQuery(function(){
			var char_array = [];
			var list_nav_html = [];
			var all_char_array = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
			jQuery('.category-groups.columns-3 > h3 > a').each(function(index, value){
				var txt_val = jQuery(this).text().charAt(0).toLowerCase();
				if (jQuery.inArray(txt_val, char_array) == -1) {
                    char_array.push(txt_val);
                    //jQuery(this).parent().parent().attr('data-char', txt_val);
                    jQuery(this).parent().parent().attr('id', "data_char_"+txt_val);
                }
			});
			if(char_array.length > 0){
				list_nav_html += '<ul class="list_nav">';
				jQuery.each(all_char_array, function(char_index, char_value){
					if (jQuery.inArray(char_value, char_array) !== -1) {
						list_nav_html += '<li class="active" data-char_to="'+char_value+'">'+char_value+'</li>';
					} else {
						list_nav_html += '<li class="disabled" data-char_to="'+char_value+'">'+char_value+'</li>';
					}
					
				});
				list_nav_html += '</ul>';
			}
			jQuery('#list_nav_container').html(list_nav_html);
			jQuery(document).on('click', '#list_nav_container ul li.active', function(){
				var scroll_char_to = jQuery(this).data('char_to');
				var scroll_offset = jQuery('#header').innerHeight();
				jQuery('html,body').animate({scrollTop: jQuery("#data_char_"+scroll_char_to).offset().top - scroll_offset},'slow');
			});
		});
		
	</script>
	<?php
}
add_action('wp_footer', 'scroll_chr_nav', 99);

function scroll_list_nav_style(){
	?>
	<style type="text/css">
	#list_nav_container ul{display: block; margin: 0; padding: 0; list-style: none; text-transform: uppercase; font-weight: bold;}
	#list_nav_container ul:after{content: ' '; clear: both;}
	#list_nav_container ul li {display: inline-block; padding: 5px; margin: 0 3px; }
	#list_nav_container ul li.active {cursor: pointer; color: #333; }
	#list_nav_container ul li.disabled {color: #ccc; cursor: not-allowed;}
	#list_nav_container ul li:before{content: none;}
	.share-email .sd-button {background: #00ADD0 !important;}
	a.share-email{color: #fff !important;}
	#header{height: 127px !important; line-height:44px !important;}
	.top-header-menu ul li{display: inline-block; list-style: none; margin-right: 50px;}
	.top-header-menu ul li a{color: #fff !important; font-size: 14px;}
	.top-header-menu{background-color: #262626; height: 47px;}
	.container.main-menu{margin-top:10px;}
	@media (max-width: 991px) {.top-header-menu{display: none;}}
	</style>
	<?php
}
add_action('wp_head', 'scroll_list_nav_style' ,0);

add_filter('wpseo_opengraph_image', 'category_image');
function category_image($image) {
	global $post;
	if(has_post_thumbnail($post)){
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID);
		$image_attributes = wp_get_attachment_image_src($post_thumbnail_id, 'full');
		$image = $image_attributes[0];
	}
	return $image;
}
?>
