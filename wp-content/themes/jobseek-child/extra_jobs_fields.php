<?php
add_filter( 'submit_job_form_fields', 'frontend_salary_rate_field', 11 );
function frontend_salary_rate_field($fields){
	$salary_fields = array(
		'job_salary_max_rate' => array(
			'label'       => __( 'Maximum rate/h ($)', 'jobseek' ),
			'type'        => 'text',
			'required'    => false,
			'placeholder' => 'e.g. 50',
			'priority'    => 7
		),
		'job_salary_min_rate' => array(
			'label'       => __( 'Minimum rate/h ($)', 'jobseek' ),
			'type'        => 'text',
			'required'    => false,
			'placeholder' => 'e.g. 20',
			'priority'    => 7
		),
		'job_salary_max' => array(
			'label'       => __( 'Maximum Salary ($)', 'jobseek' ),
			'type'        => 'text',
			'required'    => false,
			'placeholder' => 'e.g. 50000',
			'priority'    => 8
		),
		'job_salary' => array(
			'label'       => __( 'Minimum Salary ($)', 'jobseek' ),
			'type'        => 'text',
			'required'    => false,
			'placeholder' => 'e.g. 20000',
			'priority'    => 8
		),
	);
	$job = $fields['job'];
	$fields['job'] = array_merge($job, $salary_fields);
	return $fields;
}
/*function frontend_salary_rate_field($fields){
	//$fields['job'] = array(
	$fields['job']['job_salary_min_rate'] = array(
		'label'       => __( 'Minimum rate/h ($)', 'jobseek' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => 'e.g. 20',
		'priority'    => 7
	);
	$fields['job']['job_salary_max_rate'] = array(
		'label'       => __( 'Maximum rate/h ($)', 'jobseek' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => 'e.g. 50',
		'priority'    => 7
	);
	$fields['job']['job_salary'] = array(
		'label'       => __( 'Minimum Salary ($)', 'jobseek' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => 'e.g. 20000',
		'priority'    => 7
	);
	$fields['job']['job_salary_max'] = array(
		'label'       => __( 'Maximum Salary ($)', 'jobseek' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => 'e.g. 50000',
		'priority'    => 7
	);
	return $fields;
}*/

//add_filter( 'submit_job_form_fields', 'frontend_add_max_salary_field', 11 );

function frontend_add_max_salary_field( $fields ) {
  $fields['job']['job_salary_max'] = array(
    'label'       => __( 'Maximum Salary ($)', 'jobseek' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'e.g. 50000',
    'priority'    => 7
  );
  return $fields;
}

// Add the field to admin

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_max_salary_field' );

function admin_add_max_salary_field( $fields ) {
  $fields['_job_salary_max'] = array(
    'label'       => __( 'Maximum Salary ($)', 'jobseek' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 50000',
    'description' => ''
  );
  return $fields;
}

// Display "Salary" on the single job page

add_action( 'single_job_listing_meta_end', 'display_job_max_salary_data' );
add_action( 'job_listing_meta_start', 'display_job_max_salary_data' );

function display_job_max_salary_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary_max', true );

  $thousands_separator = get_theme_mod('thousands_separator', ',');
  $sign_before = get_theme_mod('sign_before', '$');
  $sign_after = get_theme_mod('sign_after', '');
  $salary_values = get_theme_mod('salary_values', 'numeric');

  if ($salary_values == 'numeric') {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="max_salary">' . __( 'Maximum Salary:', 'jobseek' ) . ' ' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="max_salary">' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>');
    }

  } else {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="max_salary">' . __( 'Maximum Salary:', 'jobseek' ) . ' ' . esc_html( $salary ) . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="max_salary">' . esc_html( $salary ) . '</li>');
    }

  }

}

//add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );

function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Minimum Salary ($)', 'jobseek' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'e.g. 20000',
    'priority'    => 7
  );
  return $fields;
}

// Add the field to admin

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );

function admin_add_salary_field( $fields ) {
  $fields['_job_salary'] = array(
    'label'       => __( 'Minimum Salary ($)', 'jobseek' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 20000',
    'description' => ''
  );
  return $fields;
}

// Display "Salary" on the single job page

add_action( 'single_job_listing_meta_end', 'display_job_salary_data' );
add_action( 'job_listing_meta_start', 'display_job_salary_data' );

function display_job_salary_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary', true );

  $thousands_separator = get_theme_mod('thousands_separator', ',');
  $sign_before = get_theme_mod('sign_before', '$');
  $sign_after = get_theme_mod('sign_after', '');
  $salary_values = get_theme_mod('salary_values', 'numeric');

  if ($salary_values == 'numeric') {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="salary">' . __( 'Minimum Salary:', 'jobseek' ) . ' ' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="salary">' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>');
    }

  } else {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="salary">' . __( 'Minimum Salary:', 'jobseek' ) . ' ' . esc_html( $salary ) . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="salary">' . esc_html( $salary ) . '</li>');
    }

  }

}

//add_filter( 'submit_job_form_fields', 'frontend_add_max_rate_salary_field' );

function frontend_add_max_rate_salary_field( $fields ) {
  $fields['job']['job_salary_max_rate'] = array(
    'label'       => __( 'Maximum rate/h ($)', 'jobseek' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'e.g. 50',
    'priority'    => 7
  );
  return $fields;
}

// Add the field to admin

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_max_rate_salary_field' );

function admin_add_max_rate_salary_field( $fields ) {
  $fields['_job_salary_max_rate'] = array(
    'label'       => __( 'Maximum rate/h ($)', 'jobseek' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 50',
    'description' => ''
  );
  return $fields;
}

add_action( 'single_job_listing_meta_end', 'display_job_max_rate_data' );
add_action( 'job_listing_meta_start', 'display_job_max_rate_data' );

function display_job_max_rate_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary_max_rate', true );

  $thousands_separator = get_theme_mod('thousands_separator', ',');
  $sign_before = get_theme_mod('sign_before', '$');
  $sign_after = get_theme_mod('sign_after', '');
  $salary_values = get_theme_mod('salary_values', 'numeric');

  if ($salary_values == 'numeric') {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="max_rate">' . __( 'Maximum rate/h:', 'jobseek' ) . ' ' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="max_rate">' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>');
    }

  } else {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="max_rate">' . __( 'Maximum rate/h:', 'jobseek' ) . ' ' . esc_html( $salary ) . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="max_rate">' . esc_html( $salary ) . '</li>');
    }

  }

}

//add_filter( 'submit_job_form_fields', 'frontend_add_min_rate_salary_field' );

function frontend_add_min_rate_salary_field( $fields ) {
  $fields['job']['job_salary_min_rate'] = array(
    'label'       => __( 'Minimum rate/h ($)', 'jobseek' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'e.g. 20',
    'priority'    => 7
  );
  return $fields;
}

// Add the field to admin

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_min_rate_salary_field' );

function admin_add_min_rate_salary_field( $fields ) {
  $fields['_job_salary_min_rate'] = array(
    'label'       => __( 'Minimum rate/h ($)', 'jobseek' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 20',
    'description' => ''
  );
  return $fields;
}

add_action( 'single_job_listing_meta_end', 'display_job_min_rate_data' );
add_action( 'job_listing_meta_start', 'display_job_min_rate_data' );

function display_job_min_rate_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary_min_rate', true );

  $thousands_separator = get_theme_mod('thousands_separator', ',');
  $sign_before = get_theme_mod('sign_before', '$');
  $sign_after = get_theme_mod('sign_after', '');
  $salary_values = get_theme_mod('salary_values', 'numeric');

  if ($salary_values == 'numeric') {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="min_rate">' . __( 'Minimum rate/h:', 'jobseek' ) . ' ' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="min_rate">' . $sign_before . esc_html( number_format($salary, 0, '.', $thousands_separator) ) . $sign_after . '</li>');
    }

  } else {

    if ( isset($salary) && !empty($salary) && is_single() ) {
      echo '<li class="min_rate">' . __( 'Minimum rate/h:', 'jobseek' ) . ' ' . esc_html( $salary ) . '</li>';
    } else if ( isset($salary) && !empty($salary) ) {
      echo ('<li class="min_rate">' . esc_html( $salary ) . '</li>');
    }

  }

}
/*
Minimum rate/h ($) (optional)

e.g. 20
Maximum rate/h ($) (optional)

e.g. 50
Minimum Salary ($) (optional)

e.g. 20000
Maximum Salary ($) (optional)

*/