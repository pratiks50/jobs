/**
 * GMW main map function
 * @param gmwForm
 */
function gjmMapInit( mapObject, mapId ) {
	
	//make sure the map element exists to prevent JS error
	if ( !jQuery( '#' + mapObject['mapElement'] ).length ) {
		return;
	}
	
	if ( typeof gjmMapElements[mapId] === 'undefined' ) {

		gjmMapElements[mapId] = gjmMapElements[mapId];

		//add map obejct to global maps object
		gjmMapElements[mapId] = {
			mapId:mapId,
			map: false,
			markers:[],
			userPosition:false,
			userMarker:false,
			clusters:false,
			spiderfier:false,
			markerClicked:false,
			locationIW:false,
			userIW: false,
			bound:[],
			prevLocations: [],
			polylines : []
		};

		//initiate map options
		mapObject['mapOptions']['zoom'] 	 = ( mapObject['zoomLevel'] == 'auto' ) ? 13 : parseInt( mapObject['zoomLevel'] );
		mapObject['mapOptions']['center'] 	 = new google.maps.LatLng( mapObject['userPosition']['lat'], mapObject['userPosition']['lng'] );
		mapObject['mapOptions']['mapTypeId'] = google.maps.MapTypeId[mapObject['mapOptions']['mapTypeId']];
									
		gjmMapElements[mapId]['map'] = new google.maps.Map(document.getElementById( mapObject['mapElement'] ), mapObject['mapOptions'] );

		//after map was created
		google.maps.event.addListenerOnce( gjmMapElements[mapId]['map'], 'idle', function(){	
			 
			//fadeout the map loader if needed
			if ( mapObject['mapLoaderElement'] != false ) {
				jQuery(mapObject['mapLoaderElement']).fadeOut(1000);
			}
			
			//create map expand toggle if needed
			if ( mapObject['resizeMapElement'] != false ) {
			
				gjmMapElements[mapId]['resizeMapControl'] = document.getElementById(mapObject['resizeMapElement']);
				gjmMapElements[mapId]['resizeMapControl'].style.position = 'absolute';	
				gjmMapElements[mapId]['map'].controls[google.maps.ControlPosition.TOP_RIGHT].push(gjmMapElements[mapId]['resizeMapControl']);			
				gjmMapElements[mapId]['resizeMapControl'].style.display = 'block';
			
				//expand map function		    	
		    	jQuery('#'+mapObject['resizeMapElement']).click(function() {
		    		
		    		var mapCenter = gjmMapElements[mapId]['map'].getCenter();
		    		jQuery(this).closest('.'+mapObject.prefix+'-map-wrapper').toggleClass(mapObject.prefix+'-expanded-map');          		
		    		jQuery(this).toggleClass('gjm-icon-resize-full').toggleClass('gjm-icon-resize-small');
		    		
		    		setTimeout(function() { 			    		
		    			google.maps.event.trigger(gjmMapElements[mapId]['map'], 'resize');
		    			gjmMapElements[mapId]['map'].setCenter(mapCenter);							
					}, 100);            		
		    	});
			}
		});
	} 

	function group_markers() {

		//initiate markers clusterer or spiderfier if needed
	    if ( mapObject['markersDisplay'] == 'markers_clusterer' && typeof MarkerClusterer === 'function' ) {
	    	
	    	//remove clusters if exist
	    	if ( gjmMapElements[mapId]['clusters'] != false ) {	
	    		gjmMapElements[mapId]['clusters'].clearMarkers();
	    		//gjmMapElements[mapId]['clusters'].setMap(null);
	    	}
	    	
	    	// make sure its not set to discontinued path
	    	if ( clusterImage == 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m' ) {
	    		clusterImage = 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m';
	    	}

			gjmMapElements[mapId]['clusters'] = new MarkerClusterer( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['markers'], { 
				imagePath: clusterImage,
				maxZoom : 15 
			} );
			 
		} else if ( mapObject['markersDisplay'] == 'markers_spiderfier' && typeof OverlappingMarkerSpiderfier === 'function' ) {
			
			gjmMapElements[mapId]['spiderfier'] = new OverlappingMarkerSpiderfier(gjmMapElements[mapId]['map'], {legWeight:2} );		
			gjmMapElements[mapId]['spiderfier'].addListener('click', function( markerClicked , event) {
				iwOnClick( markerClicked );
			});	
		} 	
	}

	gjmMapElements[mapId]['bounds'] = new google.maps.LatLngBounds();

	//remove existing markers
	for ( var i = 0; i < gjmMapElements[mapId]['markers'].length + 1 ; i++ ) {

		//remove each marker from the map
		if ( i < gjmMapElements[mapId]['markers'].length ) {
			if ( typeof gjmMapElements[mapId]['markers'][i] !== 'undefined' ) {
				gjmMapElements[mapId]['markers'][i].setMap(null);	
			}
		//clear markers array
		} else {
			gjmMapElements[mapId]['markers'] = [];
			group_markers();
		}
	} 

	for ( var i = 0; i < gjmMapElements[mapId]['polylines'].length + 1 ; i++ ) {

		//remove each marker from the map
		if ( i < gjmMapElements[mapId]['polylines'].length ) {
			if ( typeof gjmMapElements[mapId]['polylines'][i] !== 'undefined' ) {
				gjmMapElements[mapId]['polylines'][i].setMap( null );	
			}
		//clear markers array
		} else {
			gjmMapElements[mapId]['polylines'] = [];
		}            
	}

	// only if not using pagination. We need to appened markers 
	if ( ! formPagin ) {
	    
	    if ( gjmMapElements[mapId]['prevLocations'] == 0 ) {

	    	gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];

	    } else {

	    	if ( appendResults ) {
				
				temLoc = jQuery.merge( mapObject['locations'], gjmMapElements[mapId]['prevLocations'] );

	    		gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];

	    		mapObject['locations'] = temLoc;
			} else {
				gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];
			}
	    }
	}

	// loop through locations
	for ( i = 0; i < mapObject['locations'].length + 1 ; i++ ) {  
		
		//create markers
		if ( i < mapObject['locations'].length ) {

			//make sure location has coordinates to prevent JS error
			if ( mapObject['locations'][i]['lat'] == undefined || mapObject['locations'][i]['long'] == undefined || mapObject['locations'][i]['lat'] == '0.000000' || mapObject['locations'][i]['long'] == '0.000000' )
				continue;
			
			var gjmLocation = new google.maps.LatLng( mapObject['locations'][i]['lat'], mapObject['locations'][i]['long'] );
			
			// only if not using markers spiderfeir
			if ( mapObject['markersDisplay'] != 'markers_spiderfier' ) {

				// check if marker with the same location already exists
				// if so, we will move it a bit
	            if ( gjmMapElements[mapId]['bounds'].contains( gjmLocation ) ) {
	                
	                // do the math     
	                var a = 360.0 / mapObject['locations'].length;
	                var newLat = gjmLocation.lat() + - .000025 * Math.cos( ( +a*i ) / 180 * Math.PI );  //x
	                var newLng = gjmLocation.lng() + - .000025 * Math.sin( ( +a*i )  / 180 * Math.PI );  //Y
	                var newPosition = new google.maps.LatLng( newLat,newLng );

	                // draw a line between the original location 
	                // to the new location of the marker after it moves
	                var polyline = new google.maps.Polyline({
					    path: [
					        gjmLocation, 
					        newPosition
					    ],
					    strokeColor: "#FF0000",
					    strokeOpacity: 1.0,
					    strokeWeight: 2,
					    map: gjmMapElements[mapId]['map']
					});

	                gjmMapElements[mapId]['polylines'].push( polyline );

					var gjmLocation = newPosition;
	            }
	        }

			gjmMapElements[mapId]['bounds'].extend( gjmLocation );

		    //create marker
			gjmMapElements[mapId]['markers'][i] = new google.maps.Marker({
				position: gjmLocation,
				icon:mapObject['locations'][i]['mapIcon'],
				map:gjmMapElements[mapId]['map'],
				id:i 
			});
			
			 //add marker to clusterer if needed
			if ( mapObject['markersDisplay'] == 'markers_clusterer' ) {	

				gjmMapElements[mapId]['clusters'].addMarker( gjmMapElements[mapId]['markers'][i] );		

				//initiae marker click
				google.maps.event.addListener( gjmMapElements[mapId]['markers'][i], 'click', function() {
					iwOnClick( this )
				});

			//add marker to spiderfier if needed
			} else if ( mapObject['markersDisplay'] == 'markers_spiderfier' ) {	

				gjmMapElements[mapId]['spiderfier'].addMarker( gjmMapElements[mapId]['markers'][i] );
				gjmMapElements[mapId]['markers'][i].setMap(gjmMapElements[mapId]['map']);	
				
			} else {		

				gjmMapElements[mapId]['markers'][i].setMap(gjmMapElements[mapId]['map'] );

				google.maps.event.addListener( gjmMapElements[mapId]['markers'][i], 'click', function() {
					iwOnClick( this )
				});			
			}

		//when done creating the markers continue
		} else {

			//remove existing user marker
			if ( gjmMapElements[mapId]['userMarker'] != false ) {
				gjmMapElements[mapId]['userMarker'].setMap(null);
				gjmMapElements[mapId]['userMarker']   = false;
				gjmMapElements[mapId]['userPosition'] = false;
			}

			//create user's location marker
			if ( mapObject['userPosition']['lat'] != false && mapObject['userPosition']['lng'] != false && mapObject['userPosition']['mapIcon'] != false ) {
			
				//user's location
				gjmMapElements[mapId]['userPosition'] = new google.maps.LatLng( mapObject['userPosition']['lat'], mapObject['userPosition']['lng'] );
				
				//append user's location to bounds
				gjmMapElements[mapId]['bounds'].extend( gjmMapElements[mapId]['userPosition'] );
				
				//create user's marker
				gjmMapElements[mapId]['userMarker'] = new google.maps.Marker({
					position : gjmMapElements[mapId]['userPosition'],
					map      : gjmMapElements[mapId]['map'],
					icon     : mapObject['userPosition']['mapIcon']
				});
				
				/* 
				var circle = new google.maps.Circle({
				  map: gjmMapElements[mapId]['map'],
				  radius: 200,    // 10 miles in metres
				  fillColor: '#AA0000'
				});

				circle.bindTo( 'center', gjmMapElements[mapId]['userMarker'], 'position' );
				*/

				//create user's marker info-window
				if ( mapObject['userPosition']['iwContent'] != null ) {

					gjmMapElements[mapId]['userIW'] = new google.maps.InfoWindow({
						content: mapObject['userPosition']['iwContent']
					});
				      					
					if ( mapObject['userPosition']['iwOpen'] == true ) {
						gjmMapElements[mapId]['userIW'].open( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['userMarker'] );
					}
					
				    google.maps.event.addListener( gjmMapElements[mapId]['userMarker'], 'click', function() {
				    	gjmMapElements[mapId]['userIW'].open( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['userMarker'] );
				    });     
				}
			} 

			//zoom map to fit all markers
			if ( gjmMapElements[mapId]['markers'].length == 0 ) {

				gjmMapElements[mapId]['map'].setZoom(1);

			} else if ( mapObject['locations'].length == 1 && gjmMapElements[mapId]['userPosition'] == false ) {

				gjmMapElements[mapId]['map'].setZoom(13);
				gjmMapElements[mapId]['map'].panTo(gjmMapElements[mapId]['markers'][0].getPosition());

			} else if ( mapObject['zoomLevel'] == 'auto' || gjmMapElements[mapId]['userPosition'] == false  ) { 
			
				gjmMapElements[mapId]['map'].fitBounds(gjmMapElements[mapId]['bounds']);
			}
		}
	}
	
	//on marker click function
	function iwOnClick( markerClicked ) {

		if ( mapObject['locations'][markerClicked.id]['info_window_content']  ) {
				
			if ( gjmMapElements[mapId]['locationIW'] ) {
				gjmMapElements[mapId]['locationIW'].close();
				gjmMapElements[mapId]['locationIW'] = null;
			}
			
			gjmMapElements[mapId]['locationIW'] = new google.maps.InfoWindow({
				content: mapObject['locations'][markerClicked.id]['info_window_content']
			});
	
			gjmMapElements[mapId]['locationIW'].open(gjmMapElements[mapId]['map'], markerClicked);
		}
	}
}

jQuery(document).ready(function($){ 	
	
	formPagin = ( typeof formFilters === 'undefined' ) ? false : formFilters.show_pagination;

	if ( typeof formPagin === 'undefined' || ( formPagin != 'true' && formPagin != 'yes' && formPagin != 1 ) ) {
		
		formPagin = false;

		$( '.job_listings, .resumes' ).on( 'update_results', function( event, page, append ) {
			if ( append ) {
				appendResults = true;
			} else {
				appendResults = false;
			}
		});
	}	

	// hide map if no results found.
	// temporary solution untill we come up with something better.
	$( document ).ajaxStop(function( event ) {
	  if ( $('.no_job_listings_found, .no_resumes_found').length  ) {
	  	$( ".no_job_listings_found, .no_resumes_found" ).each(function( index ) {
	  		mapId = $(this).closest( 'div.job_listings, div.resumes' ).find( '.map-wrapper').slideUp();
	  	});
	  }
	});

	if ( typeof gjmMapObjects == 'undefined' ) 
		return false;

	gjmMapsObjectInit( gjmMapObjects );
});

function gjmMapsObjectInit( gjmMapObjects ) {
	
	//create global maps object only once on page load
	//the maps object will allow us to have multiple maps on the screen
	if ( typeof gjmMapElements === 'undefined' ) {
		gjmMapElements = {};
	}

	jQuery.each( gjmMapObjects, function( index, mapObject ) {

		if ( mapObject['triggerMap'] == true ) {
			
			//if map element is hidden show it first
			if ( mapObject['hiddenElement'] != false && jQuery(mapObject['hiddenElement']).is(':hidden') ) {
		
				jQuery(mapObject['hiddenElement']).slideToggle( 'fast', function() {
					gjmMapInit( mapObject, index );
				});

			} else {

				gjmMapInit( mapObject, index );
			} 	
		}
	});		
}