<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) )
	exit;
	
/**
 * Jobs global map shortcode
 * @param  array $atts shortcode aruments
 * @return void
 */
function gjm_jobs_global_map( $atts ) {

	//make sure the class of the item exists
	if ( !class_exists( "GJM_Global_Map" ) )
		return;

	$class_name = "GJM_Global_Map";
	
	$global_map = new $class_name( $atts );

	//output the map
	return $global_map->display();
}
add_shortcode( 'gjm_jobs_map', 'gjm_jobs_global_map' );