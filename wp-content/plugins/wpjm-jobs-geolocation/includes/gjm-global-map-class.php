<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Global map class
 * @since 1.7
 */
class GJM_Global_Map {

	/**
	 * @since 1.7
	 * Public $args
	 * Array of Incoming arguments
	 */
	protected $args = array(
		'element_id'	  => 0,
		'addon'			  => 'gjm',
		'post_types'	  => 'job_listing',
		'prefix'		  => 'gjm',
		'map_height'      => '250px',
		'map_width'       => '250px',
		'map_type'        => 'ROADMAP',
		'results_count'	  => 200,
		'scroll_wheel'	  => 1,
		'group_markers'	  => 'markers_clusterer',
		'user_marker' 	  => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
		'location_marker' => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
		'clusters_path'   => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m'
	);

	/**
	 * @since 1.7
	 * Public $args
	 * Array for child class to extends the main array above
	 */
	protected $ext_args = array();

	/**
	 * [__construct description]
	 * @param array $atts [description]
	 */
	function __construct( $atts=array() ) {
		
		//extend the default args
		$this->args = array_merge( $this->args, $this->ext_args );
		
		//get the shortcode atts
		$this->args = shortcode_atts( $this->args, $atts );
		
		//set random element id if not exists
		$this->args['element_id'] = ( !empty( $this->args['element_id'] ) ) ? $this->args['element_id'] : rand( 10, 1000 );
	}
	
	/**
	 * Output global map
	 * @return void
	 */
	public function display() {
		
		$queryArgs = array(
			'post_type'           => array( $this->args['post_types'] ),
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $this->args['results_count'],
			'tax_query'           => array(),
			'meta_query'          => array()
		);

		add_filter( 'posts_clauses', array( $this, 'query_clauses' ) );
		
		$results = new WP_Query( apply_filters( $this->args['prefix'].'_global_map_args' ,$queryArgs, $this->args ) );

		remove_filter( 'posts_clauses', array( $this, 'query_clauses' ) );
				
		//if no results, abort!
		if ( empty( $results->posts ) )
			return;
		
		//loop through results and add info window content to each $post object
		foreach ( $results->posts as $key => $post ) {	

			// info window content	
			$results->posts[$key]->info_window_content = ( $this->args['addon'] == 'gjm' ) ? gjm_info_window_content( $post, true ) : grm_info_window_content( $post, true );
			
			// map icon
			$mapIcon = ! empty( $this->args['location_marker'] ) ? $this->args['location_marker'] : 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
			$results->posts[$key]->mapIcon = apply_filters( 'gjm_global_map_icon', $mapIcon, $post );
		}

		//map arguments
    	$mapArgs = array(
    		'mapId'		 => $this->args['element_id'],
			'prefix'	 => $this->args['prefix'],
			'addon'		 => $this->args['addon'],
			'map_width'  => $this->args['map_width'],
			'map_height' => $this->args['map_height'],
    	);

    	//Creae the map element
		$map = gjm_get_results_map( $mapArgs );
				
		//map options - pass to via global map object to JavaScript which will display the map
		$mapArgs = array(
			'mapId'				=> $this->args['element_id'],
			'mapType'			=> $this->args['addon'],
			'prefix'			=> $this->args['prefix'],
			'locations'			=> $results->posts,
			'infoWindowType'	=> 'normal',
			'zoomLevel'			=> 'auto',
			'mapOptions'		=> array(
				'mapTypeId'		=> $this->args['map_type'],
				'scrollwheel'	=> $this->args['scroll_wheel']
			),
			'markersDisplay'	=> $this->args['group_markers']
		);

		gjm_new_map_element( $mapArgs );
		
		if ( empty( $this->args['clusters_path'] ) || $this->args['clusters_path'] == 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m' ) {
			$this->args['clusters_path'] = 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m';
		}

		$this->args['clusters_path'] = apply_filters( 'gjm_global_map_clusters_path', $this->args['clusters_path'], $this );
		wp_localize_script( 'gmw-marker-clusterer', 'clusterImage', $this->args['clusters_path'] );

        return $map;
	}
		
	/**
	 * Global Map clauses
	 * Modify the wp_query to display jobs with locations
	 * @param  object $clauses WP_Query clauses
	 * @return void          
	 */
	public function query_clauses( $clauses ) {
		
		global $wpdb;
		
		// join the location table into the query
		$clauses['join']   .= " INNER JOIN " . $wpdb->prefix . "places_locator gmwlocations ON $wpdb->posts.ID = gmwlocations.post_id ";
		$clauses['fields']  = "$wpdb->posts.*, gmwlocations.lat, gmwlocations.long, gmwlocations.address, gmwlocations.formatted_address ";
		$clauses['where']  .= " AND ( gmwlocations.lat != '0.000000' AND gmwlocations.long != '0.000000' ) ";
		
		return apply_filters( $this->args['prefix'].'_global_map_query_clauses', $clauses );	
	}
}