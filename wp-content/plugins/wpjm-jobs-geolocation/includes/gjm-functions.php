<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * GJM Labels
 * @return labels to be used in the plugin. Labels can be modified using the filter 'gjm_form_labels'
 */
function gjm_labels() {
	$labels = apply_filters( 'gjm_form_labels', array(
		'miles' 	 => __( 'Miles', 'GJM' ),
		'kilometers' => __( 'Kilometers', 'GJM' ),
		'within'	 => __( 'Within', 'GJM' ),

		'orderby_filters'	=> array(
			'label'	 	 => __( 'Order by', 'GJM' ),
			'distance' 	 => __( 'Distance', 'GJM' ),
			'title' 	 => __( 'Title', 'GJM' ),
			'featured' 	 => __( 'Featured', 'GJM' ),
			'date' 		 => __( 'Date', 'GJM' ),
			'modified'	 => __( 'Last Updated', 'GJM' ),
			'ID'		 => __( 'ID', 'GJM' ),
			'parent'	 => __( 'Parent', 'GJM' ),
			'rand'		 => __( 'Random', 'GJM' ),
			'name'		 => __( 'Name', 'GJM' ),
		),
		'search_result'	=> array(
			'company'	 => __( 'Company:', 'GJM' ),
			'address'    => __( 'Address:', 'GJM' ),
			'job_type'	 => __( 'Job Type:', 'GJM' ),
			'posted'	 => __( 'Posted %s ago', 'GJM' ),
			'mi'		 => __( 'mi', 'GJM' ),
			'km'		 => __( 'km', 'GJM ' )
		),
		'resize_map' => __( 'Resize map', 'GJM' ),
	) ); 

	return $labels;
}

/**
 * GJM - Display results map 
 * @param  array $args 
 * @return map element      
 */
function gjm_get_results_map( $atts ) {

	//enqueue the map script
	if ( !wp_script_is( 'gjm-map', 'enqueued' )  ) {	
		wp_enqueue_script( 'gjm-map' );
	}

	//default map settings
	$args = shortcode_atts( array(
		'mapId'			=> rand( 5, 100 ),
		'prefix'		=> 'gjm',
		'addon'			=> 'gjm',
		'map_width'     => '100%',
		'map_height'    => '300px',
		'init_map_show' => true,
	), $atts );

	$display = ( empty( $args['init_map_show'] ) ) ? 'display:none;' : '';
	$args    = apply_filters( $args['prefix']."_map_output_args", $args );

   	$args = array_map( 'esc_attr', $args );

    $output['open']    		  = "<div id=\"{$args['prefix']}-map-wrapper-{$args['mapId']}\" class=\"{$args['prefix']}-map-wrapper map-wrapper\"  data-map-id=\"{$args['mapId']}\" style=\"{$display}width:{$args['map_width']};height:{$args['map_height']};\">";
    $output['resize_toggle']  = '<span id="' . $args['prefix'] . '-resize-map-trigger-' . $args['mapId'] . '" class="' . $args['prefix'] . '-resize-map-trigger gjm-icon-resize-full" style="display:none;" title="'.__( 'Resize map','GMW' ) .'"></span>';
    $output['map']     		  = "<div id=\"{$args['prefix']}-map-{$args['mapId']}\" data-map-id=\"{$args['mapId']}\" class=\"{$args['prefix']}-map {$args['prefix']}-location-map\" style=\"width:100%; height:100%\"></div>";
    $output['loader'] 	  	  = "<i id=\"{$args['prefix']}-map-loader-{$args['mapId']}\" class=\"{$args['prefix']}-map-loader gjm-icon-spin-thin animate-spin\"></i>";
    $output['close']   		  = '</div>';
			
    //modify the map element
    $output = apply_filters( $args['prefix']."_map_output", $output, $args );
    
    return implode( ' ', $output );
}

/**
 * GJM map element
 * @param  array   $args   Array of options that creates the map
 * @param  boolean $return 		
 * @return void
 */
function gjm_new_map_element( $args, $return = false ) {

	global $gjmMapElements;
	
	//check if global already set
	if ( empty( $gjmMapElements ) ) {
		$gjmMapElements = array();
	}
	
	$mapID = ( ! empty( $args['mapId'] ) ) ? $args['mapId'] : rand( 100, 1000 );

	//default map args
	$defaultArgs = array(
		'mapId' 	 		=> $mapID,
		'mapType'			=> 'na',
		'prefix'			=> 'na',
		'mapElement' 		=> $args['prefix'].'-map-'.$mapID,
		'triggerMap'		=> true,
		'form' 		 		=> false,
		'hiddenElement' 	=> '#'.$args['prefix'].'-map-wrapper-'.$mapID,					
		'mapLoaderElement' 	=> '#'.$args['prefix'].'-map-loader-'.$mapID,
		'locations'			=> array(),
		'infoWindowType'	=> 'normal',
		'zoomLevel'			=> 13,
		'resizeMapElement'	=> $args['prefix'].'-resize-map-trigger-'.$mapID,
		'zoomPosition'		=> false,
		'mapOptions'		=> array(
			'mapTypeId'				 => 'ROADMAP',
			'backgroundColor' 		 => '#f7f7f7',
			'disableDefaultUI' 		 => false,
			'disableDoubleClickZoom' => false,
			'draggable'				 => true,
			'maxZoom'		 		 => null,
			'minZoom'		 		 => null,
			'panControl'	 		 => true,
			'zoomControl'	 		 => true,
			'mapTypeControl' 		 => true,
			'rotateControl'  		 => true,
			'scaleControl'			 => true,
			'scrollwheel'	 		 => true,
			'streetViewControl' 	 => true,
			'styles'				 => null,
			'tilt'					 => null,
		),
		'userPosition'		=> array(
			'lat'		=> false,
			'lng'		=> false,
			'location'	=> false,
			'address' 	=> false,
			'mapIcon'	=> 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
			'iwContent' => null,
			'iwOpen'	=> true
		),
		'markersDisplay'	=> 'normal',
		'markers'			=> array(),
		'infoWindow'		=> null,
		'resizeMapControl'	=> false,
		'draggableWindow'	=> false
	);
	
	//merge default args with incoming args
	$gjmMapElements[$mapID] = array_replace_recursive( $defaultArgs, $args );

	//allow plugins modify the map
	$gjmMapElements[$mapID] = apply_filters( $args['prefix'].'_map_'.$mapID.'_element', $gjmMapElements[$mapID] );
	$gjmMapElements[$mapID] = apply_filters( $args['prefix'].'_map_element', 			$gjmMapElements[$mapID] );

	//Load marker clusterer library - to be used with premium features
	if ( $gjmMapElements[$mapID]['markersDisplay'] == 'markers_clusterer' && ! wp_script_is( 'gmw-marker-clusterer', 'enqueued' )  ) {	
		wp_enqueue_script( 'gmw-marker-clusterer' );
	}

	//load marker clusterer library - to be used with premium features
	if ( $gjmMapElements[$mapID]['markersDisplay'] == 'markers_spiderfier' && ! wp_script_is( 'gmw-marker-spiderfier', 'enqueued' )  ) {	
		wp_enqueue_script( 'gmw-marker-spiderfier' );
	}

	//pass the mapObjects to JS
	wp_localize_script( 'gjm-map', 'gjmMapObjects', $gjmMapElements );

	if ( $return ) 
		return $gjmMapElements[$mapID];
}

/**
 * Create the content of the info-window
 * @param unknown_type $post
 */
function gjm_info_window_content( $post, $gmap = false ) {
	
	//global $post;

	//get labels
	$labels = gjm_labels();

	$gmapClass = ( $gmap ) ? 'gmap' : '';

	//get the address
	$address  	   = ( !empty( $post->formatted_address ) ) ? $post->formatted_address : $post->address;
	$job_type_slug = $job_type_name = '';

	if ( get_the_job_type( $post ) ) {
		$job_type_slug = sanitize_title( get_the_job_type( $post )->slug );
		$job_type_name = sanitize_title( get_the_job_type( $post )->name );
	}

	$output['content_start']  = '<div id="gjm-info-window-wrapper-'.$post->ID.'" class="gjm-info-window-wrapper '.$gmapClass.'">';

	if ( isset( $post->distance ) ) {
		$output['distance']= '<span class="distance">'.$post->distance.'</span>';
	}

	$output['title'] 	   = '<h3 class="title"><a href="'.get_permalink( $post->ID ).'" title="'.$post->post_title.'">'.$post->post_title.'</a></h3>';
	$output['items_start'] = '<ul class="job-items">';
	$output['company'] 	   = '<li class="compny-name"><span class="label">'.esc_attr( $labels['search_result']['company'] ).' </span><span class="item">'.the_company_name( '<span class="company-name">', '<span>', false, $post ).'</span></li>';
	$output['address'] 	   = '<li class="address"><span class="label">'. esc_attr( $labels['search_result']['address'] ).' </span><span class="item">'.esc_attr( $address ).'</span></li>';
	$output['job_types']   = '<li class="job-type '.esc_attr( $job_type_slug ).'"><span class="label">'.esc_attr( $labels['search_result']['job_type'] ).' </span><span class="item">'.esc_attr( $job_type_name ).'</span></li>';
	$output['posted'] 	   = '<li class="date"><span class="item"><date>'.sprintf( $labels['search_result']['posted'], human_time_diff( get_post_time( 'U', false, $post ), current_time( 'timestamp' ) ) ).'</date></li>';
	$output['items_end']   = '</ul>';
	$output['content_end'] = '</div>';
	
	$output = apply_filters( 'gjm_info_window_content', $output, $post, $gmap );

	return implode( '', $output );
}

/**
 * Single Job Page map
 * @return void 
 */
function gjm_single_page_map() {

	global $post;

	//verfy that we are in a single post of job or resume post type
	if ( $post->post_type != 'job_listing' ) 
		return;
		
	$gjm_settings = get_option( 'gjm_options' );
	$gjm_settings = $gjm_settings['single_page'];
	
	//verfy map settings
	//if ( empty( $gjm_settings['gjm_single_map_enabled'] ) || $gjm_settings['gjm_single_map_enabled'] == 'disabled' )
		//return;
	
	global $wpdb;
	
	$gjmLocation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}places_locator WHERE post_id = %d", $post->ID ) );

	if ( empty( $gjmLocation ) || $gjmLocation->lat == '0.000000' || $gjmLocation->long == '0.000000' )
		return;

	$gjmLocation->mapIcon = $gjm_settings['gjm_single_map_location_marker'];

	//if post doen not have coordinates stop the function
	if ( !isset( $gjmLocation->lat ) || !isset( $gjmLocation->long ) )
		return;

	$element_id = rand( 10, 100 );

	//map arguments
	$mapArgs = array(
		'mapId'		 	=> $element_id,
		'prefix'	 	=> 'gjm',
		'init_map_show' => true,
		'addon'		 	=> 'gjm',
		'map_width'  	=> $gjm_settings['gjm_single_map_width'],
		'map_height' 	=> $gjm_settings['gjm_single_map_height'],
	);

	//Creae the map element
	$map = gjm_get_results_map( $mapArgs );
			
	//map options - pass to via global map object to JavaScript which will display the map
	$mapArgs = array(
		'mapId'				=> $element_id,
		'mapType'			=> 'gjm',
		'prefix'			=> 'gjm',
		'locations'			=> array( $gjmLocation ),
		'infoWindowType'	=> 'normal',
		'zoomLevel'			=> 'auto',
		'mapOptions'		=> array(
			'mapTypeId'		=> $gjm_settings['gjm_single_map_type'],
			'scrollwheel'	=> $gjm_settings['gjm_single_map_scroll_wheel']
		)
	);

	gjm_new_map_element( $mapArgs );

	// make sure only one single map page will show on the page
	remove_action( 'job_application_end',      'gjm_single_page_map' );
	remove_action( 'single_job_listing_start', 'gjm_single_page_map' );
	remove_action( 'single_job_listing_end',   'gjm_single_page_map' );

	echo $map;
}

/**
 * Single job map shortcode
 * @return [type] [description]
 */
function gjm_single_page_map_shortcode() {

	ob_start();

	gjm_single_page_map();

	$output = ob_get_contents();

	ob_end_clean();

	return $output;
}
add_shortcode( 'gjm_single_job_map', 'gjm_single_page_map_shortcode' );

$gjm_options = get_option( 'gjm_options' );

if ( ! empty( $gjm_options['single_page']['gjm_single_map_enabled'] ) && $gjm_options['single_page']['gjm_single_map_enabled'] != 'disabled' ) {

	if ( $gjm_options['single_page']['gjm_single_map_enabled'] == 'top' ) {
		add_action( 'single_job_listing_start', 'gjm_single_page_map', 50 );
	} else {
		add_action( 'single_job_listing_end', 'gjm_single_page_map', 50 );
	}
}
//add_action( 'job_application_end', 'gjm_single_page_map' );
//add_action( 'gjm_single_job_map', 'gjm_single_page_map' );

function gjm_address_autocomplete_front_end() {

	$settings = get_option( 'gjm_options' );

	if ( empty( $settings['general_settings']['gjm_address_autocomplete_form_frontend'] ) ) 
			return;

	$ac_options = array(
		'input_field'   => 'job_location',
		'country' 		=> ! empty( $settings['general_settings']['gjm_address_autocomplete_country'] ) ? $settings['general_settings']['gjm_address_autocomplete_country'] : '',
		'results_type'  => ! empty( $settings['general_settings']['gjm_address_autocomplete_results_type'] ) ? $settings['general_settings']['gjm_address_autocomplete_results_type'] : 'geocode'
	);
	
	wp_enqueue_script( 'gjm-autocomplete');
	wp_localize_script( 'gjm-autocomplete', 'AutoCompOptions', $ac_options );
}
add_action( 'submit_job_form_job_fields_start', 'gjm_address_autocomplete_front_end' );

/**
 * add new job to GJM table in database
 * @since  1.0
 * @author Eyal Fitoussi
 */
function gjm_update_location_front_end( $post_id, $values ) {

	$geolocated = get_post_meta( $post_id, 'geolocated', true );
	
	//delete location if address field empty
	if ( empty( $values['job']['job_location'] ) || empty( $geolocated ) ) {

		global $wpdb;
		$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "places_locator WHERE post_id=%d", $post_id ) );
		return;
	
	} else {
	
		$street_number = get_post_meta( $post_id, 'geolocation_street_number', true );
		$street_name   = get_post_meta( $post_id, 'geolocation_street', true );
		$street_check  = trim( $street_number.' '.$street_name );
		$street		   = ( !empty( $street_check ) ) ? $street_number.' '.$street_name : '';
		
		//Save location information to database
		global $wpdb;
		$wpdb->replace( $wpdb->prefix . 'places_locator', array(
			'post_id'           => $post_id,
			'feature'           => 0,
			'post_type'         => 'job_listing',
			'post_title'        => $values['job']['job_title'],
			'post_status'       => 'publish',
			'street_number' 	=> $street_number,
			'street_name' 		=> $street_name,
			'street' 			=> $street,
			'apt' 				=> '',
			'city' 				=> get_post_meta( $post_id, 'geolocation_city', true ),
			'state' 			=> get_post_meta( $post_id, 'geolocation_state_short', true ),
			'state_long' 		=> get_post_meta( $post_id, 'geolocation_state_long', true ),
			'zipcode' 			=> get_post_meta( $post_id, 'geolocation_postcode', true ),
			'country' 			=> get_post_meta( $post_id, 'geolocation_country_short', true ),
			'country_long' 		=> get_post_meta( $post_id, 'geolocation_country_long', true ),
			'address' 			=> ! empty( $values['job']['job_location'] ) ? $values['job']['job_location'] : '',
			'formatted_address' => get_post_meta( $post_id, 'geolocation_formatted_address', true ),
			'phone'             => '',
			'fax'               => '',
			'email'             => ! empty( $values['job']['application'] ) ? $values['job']['application'] : '',
			'website'           => ! empty( $values['job']['company_website'] ) ? $values['job']['company_website'] : '',
			'lat'               => get_post_meta( $post_id, 'geolocation_lat',  true ),
			'long'              => get_post_meta( $post_id, 'geolocation_long', true ),
			'map_icon'          => '_default.png',
		));
	}
}
add_action( 'job_manager_update_job_data', 'gjm_update_location_front_end', 20, 2 );
