<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * GRM_Admin class
 */
class GRM_Admin_Settings extends GJM_Admin_Settings {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() { 
		
		$this->settings = get_option( 'grm_options' );	
		$this->prefix = 'grm';

		//default settings
		if ( !empty( $_GET['post_type'] ) && $_GET['post_type'] == 'resume' && !empty( $_GET['page' ] ) && $_GET['page'] == 'resume-manager-settings' ) {
			add_action( 'admin_init', array( $this, 'default_options' ), 99 );
		}

		add_filter( 'resume_manager_settings', array ( $this, 'admin_settings' ) );
		add_action( 'wp_job_manager_admin_field_grm_locations_importer', array( $this, 'locations_importer_form' ) );
		
		//hook custom function that creates the importer button
		add_action( 'wp_job_manager_admin_field_grm_user_incstructions', array( $this, 'shortcode_user_instructions' ) );

		//make sure we updating WPJM settings page to also update our settings
		if ( !empty( $_POST['action'] )  && $_POST['action'] == 'update' && !empty( $_POST['option_page' ] ) && $_POST['option_page'] == 'wp-job-manager-resumes' ) {
			add_action( 'admin_init', array( $this, 'options_validate' ), 10 );		
		}	
	}
	
	public function shortcode_user_instructions() {
		
		$output  = '<p>'. __( 'There are two ways you can apply the Geolocaiton features to a resume form:</p>', 'GJM' );
		$output .= 	  '<ol>';
		$output .= 	    '<li>' . sprintf( __( 'Add the attribute %s with the value 2 to the %s shortcode ( ex. %s ) if you\'d like to apply the geolocation features based on the settings on this page.', 'GJM'), '<code>grm_use</code>' , '<code>[resumes]</code>', '<code>[resume grm_use="2"]</code>' ).'</li>';
		$output .= 		'<li>' . sprintf( __( 'Add the attribute %s with the value 1 to the %s shortcode ( ex. %s ) when you use multiple %s shortcodes and you\'d like to defined each of the shortcodes with different geolocation features. You can do so by using the other shotcode attributes that %s provides. The list of shortcode attributes can be found <a %s>here</a>.', 'GJM'), '<code>grm_use</code>', '<code>[resumes]</code>', '<code>[resumes grm_use="1"]</code>.', '<code>[resumes]</code>', 'Resume Manager Geolocation plugin', 'href="http://docs.geomywp.com/wp-job-manager-geolocation-shortcodes/" target="_blank"' ).'</li>';
		$output .= 	  '</ol>'; 
		$output .= '<hr />';

		echo $output;
	}
}
new GRM_Admin_Settings;