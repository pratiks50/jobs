<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( class_exists( 'GJM_Admin_Settings') ) :
	
/**
 * GJM_Admin class
 */
class GRM_Admin {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		$this->settings = get_option( 'grm_options' );
		
		add_action( 'admin_print_scripts-post-new.php', array( $this, 'address_autocomplete'   ), 11 );
		add_action( 'admin_print_scripts-post.php', 	array( $this, 'address_autocomplete'   ), 11 );			
		add_action( 'save_post' , 						array( $this, 'update_resume_location' ), 99 );
		add_action( 'pmxi_saved_post', 					array( $this, 'pmxi_location_update'   ), 20 );

		include( 'grm-admin-settings.php' );
	}
		
	/**
	 * Google Places address autocomplete in "Edit Job" page admin.
	 * @return void [description]
	 */
	function address_autocomplete() {

		global $post_type;

		if ( $post_type != 'resume' || empty( $this->settings['general_settings']['grm_address_autocomplete_form_admin'] ) ) 
			return;

		$ac_options = array(
			'input_field'   => '_candidate_location',
			'country' 		=> !empty( $this->settings['general_settings']['grm_address_autocomplete_country'] ) ? $this->settings['general_settings']['grm_address_autocomplete_country'] : ''
		);
		
		wp_enqueue_script( 'gjm-autocomplete');
		wp_localize_script( 'gjm-autocomplete', 'AutoCompOptions', $ac_options );
	}
	
	/**
	 * Add location data to GEo my WP table in database
	 * @param unknown_type $post_id
	 */
	public function add_location_to_db( $post_id ) {
		
		global $wpdb;
		
		$street_number = get_post_meta( $post_id, 'geolocation_street_number', true );
		$street_name   = get_post_meta( $post_id, 'geolocation_street', true );	
		$street_check  = trim( $street_number.' '.$street_name );
		$street		   = ! empty( $street_check ) ? $street_number.' '.$street_name : '';

		$wpdb->replace( $wpdb->prefix . 'places_locator',
			array(
				'post_id'			=> $post_id,
				'feature'  			=> 0,
				'post_type' 		=> $_POST['post_type'],
				'post_title'		=> $_POST['post_title'],
				'post_status'		=> $_POST['post_status'],
				'street_number' 	=> $street_number,
				'street_name' 		=> $street_name,
				'street' 			=> $street,
				'apt' 				=> '',
				'city' 				=> get_post_meta( $post_id, 'geolocation_city', true ),
				'state' 			=> get_post_meta( $post_id, 'geolocation_state_short', true ),
				'state_long' 		=> get_post_meta( $post_id, 'geolocation_state_long', true ),
				'zipcode' 			=> get_post_meta( $post_id, 'geolocation_postcode', true ),
				'country' 			=> get_post_meta( $post_id, 'geolocation_country_short', true ),
				'country_long' 		=> get_post_meta( $post_id, 'geolocation_country_long', true ),
				'address' 			=> ! empty( $_POST['_candidate_location'] ) ? $_POST['_candidate_location'] : '',
				'formatted_address' => get_post_meta( $post_id, 'geolocation_formatted_address', true ),
				'phone' 			=> '',
				'fax' 				=> '',
				'email' 			=> ! empty( $_POST['_candidate_email'] ) ? $_POST['_candidate_email'] : '',
				'website' 			=> '',
				'lat' 				=> get_post_meta( $post_id, 'geolocation_lat', true ),
				'long' 				=> get_post_meta( $post_id, 'geolocation_long', true ),
				'map_icon'  		=> '_default.png',
			)
		);
	}

	/**
	 * Update resume location when saving resume in admin
	 * @param  $post_id 
	 * @return void
	 */
	function update_resume_location( $post_id ) {
		global $post;

		if ( !isset( $_POST['post_type']) || $_POST['post_type'] != 'resume' )
			return;

		// verify nonce //
		if ( empty( $_POST['resume_manager_nonce'] ) || ! wp_verify_nonce( $_POST['resume_manager_nonce'], 'save_meta_data' ) )
			return;
	
		// Return if it's a post revision
		if ( false !== wp_is_post_revision( $post_id ) )
			return;

		// check autosave //
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;

		$geolocated = get_post_meta( $post_id, 'geolocated', true );
		
		//delete location if address field empty
		if ( empty( $_POST['_candidate_location'] ) || empty( $geolocated ) ) {
			
			global $wpdb;

			$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "places_locator WHERE post_id=%d", $post->ID ) );

			return;
		
		} else {

			self::add_location_to_db( $post_id );
			return;
		} 	
	}	

	/**
	 * Update location data when importing using WP ALL IMPORT
	 * @param unknown_type $post_id
	 */
	public function pmxi_location_update( $post_id ) {

		if ( 'resume' === get_post_type( $post_id ) ) {

			$geolocated = get_post_meta( $post_id, 'geolocated', true );

			if ( !empty( $geolocated ) ) {	
				
				self::add_location_to_db( $post_id );					
				return;
			}
		}
	}
}
new GRM_Admin;

endif;