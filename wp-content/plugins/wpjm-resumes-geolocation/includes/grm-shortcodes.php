<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) )
	exit;
	
/**
 * Jobs global map shortcode
 * @param  array $atts shortcode aruments
 * @return void
 */
function grm_resumes_global_map( $atts ) {

	//make sure the class of the item exists
	if ( !class_exists( "GJM_Global_Map" ) )
		return;

	$class_name = "GJM_Global_Map";

	$atts['element_id'] = rand( 10, 1000 );
	$atts['type']		= 'resumes';
	$atts['addon']	    = 'grm';
	$atts['post_types'] = 'resume';
	$atts['prefix']	 	= 'grm';

	$global_map = new $class_name( $atts );

	//output the map
	return $global_map->display();
}
add_shortcode( 'grm_resumes_map', 'grm_resumes_global_map' );