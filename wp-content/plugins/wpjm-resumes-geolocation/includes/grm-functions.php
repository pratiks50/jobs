<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) )
	exit;

/**
 * Trigger Google address autocomplete on resume form submission on front end
 * @return void 
 */
function grm_address_autocomplete_front_end() {

	$settings = get_option( 'grm_options' );

	if ( empty( $settings['general_settings']['grm_address_autocomplete_form_frontend'] ) ) 
			return;

	$ac_options = array(
		'input_field'   => 'candidate_location',
		'country' 		=> !empty( $settings['general_settings']['grm_address_autocomplete_country'] ) ? $settings['general_settings']['grm_address_autocomplete_country'] : ''
	);
	
	wp_enqueue_script( 'gjm-autocomplete');
	wp_localize_script( 'gjm-autocomplete', 'AutoCompOptions', $ac_options );
}
add_action( 'submit_resume_form_end', 'grm_address_autocomplete_front_end' );

/**
 * Single Resume Page map
 * @return void 
 */
function grm_single_page_map() {

	global $post;

	//verfy that we are in a single post of resume post type
	if ( $post->post_type != 'resume' ) 
		return;
	
	$gjm_settings = get_option( 'grm_options' );
	$gjm_settings = $gjm_settings['single_page'];
	
	//verfy map settings
	if ( empty( $gjm_settings['grm_single_map_enabled'] ) )
		return;
	
	global $wpdb;
	
	$gjmLocation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}places_locator WHERE post_id = %d", $post->ID ) );

	if ( empty( $gjmLocation ) || $gjmLocation->lat == '0.000000' || $gjmLocation->long == '0.000000' )
		return;

	$gjmLocation->mapIcon = $gjm_settings['grm_single_map_location_marker'];

	//if post doen not have coordinates stop the function
	if ( !isset( $gjmLocation->lat ) || !isset( $gjmLocation->long ) )
		return;

	$element_id = rand( 10, 100 );

	//map arguments
	$mapArgs = array(
		'mapId'		 	=> $element_id,
		'prefix'	 	=> 'grm',
		'init_map_show' => true,
		'addon'		 	=> 'grm',
		'map_width'  	=> $gjm_settings['grm_single_map_width'],
		'map_height' 	=> $gjm_settings['grm_single_map_height'],
	);

	//Creae the map element
	$map = gjm_get_results_map( $mapArgs );
			
	//map options - pass to via global map object to JavaScript which will display the map
	$mapArgs = array(
		'mapId'				=> $element_id,
		'mapType'			=> 'grm',
		'prefix'			=> 'grm',
		'locations'			=> array( $gjmLocation ),
		'infoWindowType'	=> 'normal',
		'zoomLevel'			=> 'auto',
		'mapOptions'		=> array(
			'mapTypeId'		=> $gjm_settings['grm_single_map_type'],
			'scrollwheel'	=> $gjm_settings['grm_single_map_scroll_wheel']
		)
	);

	gjm_new_map_element( $mapArgs );

	// make sure only one single map page will show on the page
	remove_action( 'single_resume_start', 'grm_single_page_map' );
	remove_action( 'single_resume_end',   'grm_single_page_map' );

	echo $map;
}
$grm_options = get_option( 'grm_options' );

if ( ! empty( $grm_options['single_page']['grm_single_map_enabled'] ) && $grm_options['single_page']['grm_single_map_enabled'] != 'disabled' ) {

	if ( $grm_options['single_page']['grm_single_map_enabled'] == 'top' ) {
		add_action( 'single_resume_start', 'grm_single_page_map' );
	} else {
		add_action( 'single_resume_end', 'grm_single_page_map' );
	}
}
add_action( 'grm_single_job_map','grm_single_page_map' );

/**
 * Create the content of the info-window
 * @param unknown_type $post
 */
function grm_info_window_content( $post, $gmap = false ) {
	
	//get labels
	$labels = gjm_labels();

	$gmapClass = ( $gmap ) ? 'gmap' : '';

	//get the address
	$address = ( !empty( $post->formatted_address ) ) ? $post->formatted_address : $post->address;

	$output['content_start'] = '<div id="grm-info-window-wrapper-'.$post->ID.'" class="grm-info-window-wrapper '.$gmapClass.'">';

	if ( isset( $post->distance ) ) {
		$output['distance'] = '<span class="distance">'.$post->distance.'</span>';
	}

	$output['title'] 	   = '<h3 class="title"><a href="'.get_permalink( $post->ID ).'" title="'.$post->post_title.'">'.$post->post_title.'</a></h3>';
	$output['items_start'] = '<ul class="resume-items">';
	$output['address']     = '<li class="address"><span class="label">'.__( 'Address:', 'GRM' ).'</span><span class="item">'.$address.'</span></li>';
	$output['updated']     = '<li class="date"><span class="item"><date>'.sprintf( __( 'Updated %s ago', 'GRM' ), human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ).'</date></li>';
	$output['items_end']   = '</ul>';
	$output['content_end'] = '</div>';
	
	$output = apply_filters( 'grm_info_window_content', $output, $post, $gmap );

	return implode( '', $output );
}

/**
 * add new resume to locations table in database
 * @since  1.0
 * @author Eyal Fitoussi
 */
function grm_update_location_front_end( $post_id, $values ) {

	$geolocated = get_post_meta( $post_id, 'geolocated', true );
	
	//delete location if address field empty
	if ( empty( $values['resume_fields']['candidate_location'] ) || empty( $geolocated ) ) {

		global $wpdb;
		$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "places_locator WHERE post_id=%d", $post_id ) );
		return;
	
	} else {
	
		$street_number = get_post_meta( $post_id, 'geolocation_street_number', true );
		$street_name   = get_post_meta( $post_id, 'geolocation_street', true );
		$street_check  = trim( $street_number.' '.$street_name );
		$street		   = ( !empty( $street_check ) ) ? $street_number.' '.$street_name : '';
		
		//Save location information to database
		global $wpdb;
		$wpdb->replace( $wpdb->prefix . 'places_locator', array(
			'post_id'           => $post_id,
			'feature'           => 0,
			'post_type'         => 'resume',
			'post_title'        => ! empty( $values['resume_fields']['candidate_name'] ) ? $values['resume_fields']['candidate_name'] : '',
			'post_status'       => 'publish',
			'street_number' 	=> $street_number,
			'street_name' 		=> $street_name,
			'street' 			=> $street,
			'apt' 				=> '',
			'city' 				=> get_post_meta( $post_id, 'geolocation_city', true ),
			'state' 			=> get_post_meta( $post_id, 'geolocation_state_short', true ),
			'state_long' 		=> get_post_meta( $post_id, 'geolocation_state_long', true ),
			'zipcode' 			=> get_post_meta( $post_id, 'geolocation_postcode', true ),
			'country' 			=> get_post_meta( $post_id, 'geolocation_country_short', true ),
			'country_long' 		=> get_post_meta( $post_id, 'geolocation_country_long', true ),
			'address' 			=> ! empty( $values['resume_fields']['candidate_location'] ) ? $values['resume_fields']['candidate_location'] : '',
			'formatted_address' => get_post_meta( $post_id, 'geolocation_formatted_address', true ),
			'phone'             => '',
			'fax'               => '',
			'email'             => ! empty( $values['resume_fields']['candidate_email'] ) ? $values['resume_fields']['candidate_email'] : '',
			'website'           => '',
			'lat'               => get_post_meta( $post_id, 'geolocation_lat',  true ),
			'long'              => get_post_meta( $post_id, 'geolocation_long', true ),
			'map_icon'          => '_default.png',
		)
		);
	}
}
add_action( 'resume_manager_update_resume_data', 'grm_update_location_front_end', 20, 2 );