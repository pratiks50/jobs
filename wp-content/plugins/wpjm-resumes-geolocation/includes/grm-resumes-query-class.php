<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) )
	exit;

if ( class_exists( 'GJM_Form_Query_Class' ) ) :

/**
 * GRM Query calss
 */
class GRM_Form_Query_Class extends GJM_Form_Query_Class {

	function __construct() {

		$this->settings = get_option( 'grm_options' );
		$this->prefix 	= 'grm';
		$this->labels 	= gjm_labels();
		
		add_filter( 'resume_manager_output_resumes_defaults', 			array( $this, 'default_atts' 		   ) );
		add_action( 'resume_manager_resume_filters_search_resumes_end', array( $this, 'modify_search_form' ), 8  );
		add_filter( 'get_resumes_query_args', 							array( $this, 'search_query' 	   ), 99 );
		add_filter( 'the_candidate_location', 							array( $this, 'job_distance' 	), 99, 2 );
		add_action( 'resume_manager_resume_filters_after', 				array( $this, 'results_map' 		),21 );
		add_shortcode( 'grm_results_map', 								array( $this, 'results_map_shortcode'  ) );
	}
		 
	/**
	 * info window function callback
	 * @param  object $post
	 * @return [type]       [description]
	 */
	public function info_window_content( $post ) {
		return grm_info_window_content( $post );
	}	
}
new GRM_Form_Query_Class();
endif;