<?php
/*
 Plugin Name: WP Job Manager addon - Resume Manager Geolocation
 Plugin URI: http://www.geomywp.com/add-ons/resume-manager-geolocation
 Description: Enhance WP Job Manager - Resumes plugin with geolocation features
 Version: 1.4.3
 Author: Eyal Fitoussi
 Author URI: http://www.geomywp.com
 Requires at least: 4.0
 Tested up to: 4.3.1
 Text Domain: GRM
 Domain Path: /languages/
 License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Resume_Manager_Geolocation class.
 */
class GRM_Init { 

	/**
     * __construct function.
     */
	public function __construct() {

		define( 'GRM_ITEM_NAME', 'Resume Manager Geolocation' );
		define( 'GRM_TITLE', __( 'Resume Manager Geolocation', 'GRM' ) );
		define( 'GRM_LICENSE_NAME', 'resume_manager_geo-location' );
		define( 'GRM_ITEM_ID', 8547 );
		define( 'GRM_VERSION', '1.4.3' );
		define( 'GRM_FILE', __FILE__ );
		define(	'GRM_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
		define( 'GRM_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		
		if ( !defined( 'GMW_REMOTE_SITE_URL' ) ) {
			define( 'GMW_REMOTE_SITE_URL', 'https://geomywp.com' );
		}
		
		//load textdomain
		load_plugin_textdomain( 'GRM', FALSE, dirname(plugin_basename(__FILE__)).'/languages/' );
		
		//verfiy WP JOb Manager version
		if ( !class_exists( 'GJM_Init' ) || version_compare( GJM_VERSION, '1.7', '< ' ) ) {
			add_action( 'admin_notices', array( $this, 'gjm_admin_notice' ) );      
            return;
		}

		//verfiy WP JOb Manager version
		if ( !class_exists( 'WP_Job_Manager' ) || version_compare( JOB_MANAGER_VERSION, '1.23.1', '< ' ) ) {
			add_action( 'admin_notices', array( $this, 'wpjm_admin_notice' ) );      
            return;
		} 

		//verify Resume Manager version
		if ( !class_exists( 'WP_Resume_Manager') || version_compare( RESUME_MANAGER_VERSION, '1.13', '<' ) ) {
			add_action( 'admin_notices', array( $this, 'wpjmr_admin_notice' ) );      
            return;
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ), 0 );
		//include files
		if ( is_admin() && !defined( 'DOING_AJAX' ) ) {

			//updater
        	add_filter( 'gmw_admin_addons_page', array( $this, 'addon_init' 	) );
        	add_action( 'admin_init', 			 array( $this, 'plugin_updater' ) );

        	//include admin files
			include_once GRM_PATH .'/includes/admin/grm-admin.php';
		}

		//include frontend files
        if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
			include( 'includes/grm-functions.php' );
			include( 'includes/grm-resumes-query-class.php' );
		}

		if ( !is_admin() ) {
			include( 'includes/grm-shortcodes.php' );
		}	
	}
	
	/**
	 * Admin notice if WP JOb Manager doesnt match
	 * @return void
	 */
	public function wpjm_admin_notice() {
		?>
		<div class="error">
			<p>Resume Manager Geolocation <?php printf( __( " requires <a %s>WP Job Manager</a> plugin version 1.23.1 or higher.", "GRM" ), "href=\"http://wordpress.org/plugins/wp-job-manager/\" target=\"_blank\"" ); ?></p>
		</div>
		<?php       
	}

	/**
	 * Admin notice if Resume Manager doesnt match
	 * @return void
	 */
	public function wpjmr_admin_notice() {
		?>
		<div class="error">
			<p>Resume Manager Geolocation <?php printf( __( " requires <a %s>Resume Manager</a> plugin version 1.13 or higher.", "GRM" ), "href=\"https://wpjobmanager.com/add-ons/resume-manager/\" target=\"_blank\"" ); ?></p>
		</div>
		<?php       
	}

	/**
	 * Admin notice if WP JOb Manager geolocation doesnt match
	 * @return void
	 */
	public function gjm_admin_notice() {
		?>
		<div class="error">
			<p>Resume Manager Geolocation <?php printf( __( " requires <a %s>WP Job Manager Geolocation</a> plugin version 1.7 or higher.", "GRM" ), "href=\"https://geomywp.com/add-ons/wp-job-manager-geolocation/\" target=\"_blank\"" ); ?></p>
		</div>
		<?php       
	}

	/**
	 * Include addon function.
	 * 
	 * To be used with GEO my WP
	 *
	 * @since 1.0
	 * @access public
	 * @return $addons
	 */
	public function addon_init( $addons ) {

		$addons[GRM_LICENSE_NAME] = array(
			'name' 	  		=> GRM_LICENSE_NAME,
			'item'	  		=> GRM_ITEM_NAME,
			'item_id'		=> GRM_ITEM_ID,
			'title'   		=> GRM_TITLE,
			'version' 		=> GRM_VERSION,
			'file' 	  		=> GRM_FILE,
			'basename'  	=> plugin_basename( GRM_FILE ),
			'author'  		=> 'Eyal Fitoussi',
			'desc'    		=> __( 'Enhance Resume Manager plugin with geolocation features.', 'GJM' ),
			'image'  		=> false,
			'require' 		=> array(
				'WP Job Manager plugin'        => array(
					'plugin_file' => 'wp-job-manager/wp-job-manager.php',
					'link' 		  => 'http://wordpress.org/plugins/wp-job-manager/'
				),
				'WP Job Manager Resumes plugin' => array(
					'plugin_file' => 'wp-job-manager-resumes/wp-job-manager-resumes.php',
					'link' 		  => 'http://mikejolley.com/projects/wp-job-manager/add-ons/resume-manager/'
				)
			),
			'license' 		=> true,
			'auto_trigger' 	=> true,
            'min_version'  	=> false,
            'stand_alone'  	=> true,
            'core'         	=> false,
            'gmw_version'  	=> '2.5'					
		);

		return $addons;
	}
	
	/**
	 * register scripts function.
	 *
	 * @access public
	 * @return void
	 */
	public function register_scripts() {	
		wp_enqueue_style( 'grm-frontend', GRM_URL.'/assets/css/grm.frontend.min.css', array(), GRM_VERSION );
	}

	/**
	 * Plugin updater and license key input field
	 * @return [type] [description]
	 */
	public function plugin_updater() {

		//if GEO my WP install let it do the udpating
		if ( class_exists( 'GEO_my_WP') && version_compare( GMW_VERSION, '2.6', '>' ) )
			return;

		//Check for plugin updates
        if ( class_exists( 'GMW_License' ) ) {
            new GMW_License( GRM_FILE, GRM_ITEM_NAME, GRM_LICENSE_NAME, GRM_VERSION, 'Eyal Fitoussi', GMW_REMOTE_SITE_URL, GRM_ITEM_ID  );
        }
    }
}

/**
 *  grm Instance
 *
 * @since 1.0
 * @return Resume Manager Geo-Location Instance
 */	
new GRM_Init();